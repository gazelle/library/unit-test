package net.ihe.gazelle.lib.unittest.noconstructorexception;

public class NoConstructorForTestException extends Exception {
   private static final long serialVersionUID = 3696055649973363150L;
}
