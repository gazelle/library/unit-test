package net.ihe.gazelle.lib.unittest.adapter;

import org.junit.jupiter.api.Test;
import org.junit.platform.engine.discovery.ClassNameFilter;
import org.junit.platform.launcher.Launcher;
import org.junit.platform.launcher.LauncherDiscoveryRequest;
import org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder;
import org.junit.platform.launcher.core.LauncherFactory;
import org.junit.platform.launcher.listeners.SummaryGeneratingListener;
import org.junit.platform.launcher.listeners.TestExecutionSummary;

import java.io.PrintWriter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.platform.engine.discovery.DiscoverySelectors.selectClass;
import static org.junit.platform.engine.discovery.DiscoverySelectors.selectPackage;

/**
 * Verify the Test class AnyExceptionTest is verifying properly
 *
 * @author ceoche
 */
public class AnyExceptionTestTest {

   /**
    * Verify AnyExceptionTest succeed if all required constructors are defined
    */
   @Test
   public void nominalAnyExceptionTest() {
      System.setProperty(AnyExceptionTest.PACKAGE_PROPERTY_NAME, "net.ihe.gazelle.lib.unittest.nominalexception");

      TestExecutionSummary summary = runAnyExceptionTest();

      summary.printFailuresTo(new PrintWriter(System.out));
      assertEquals(0, summary.getTotalFailureCount(), "AnyExceptionTest should succeed if all required constructors are defined");
   }

   /**
    * Verify AnyExceptionTest report 3 failure if no constructors are defined (the default public one will succeed, and the last one (String,
    * Throwable, boolean, boolean) is optional).
    */
   @Test
   public void missingConstructorExceptionTest() {
      System.setProperty(AnyExceptionTest.PACKAGE_PROPERTY_NAME, "net.ihe.gazelle.lib.unittest.noconstructorexception");

      TestExecutionSummary summary = runAnyExceptionTest();

      assertEquals(3, summary.getTotalFailureCount(),
            "AnyExceptionTest should report 3 failures when verify an exception with no declared constructor");
   }

   /**
    * Create a JUnit launcher for running AnyExceptionTest located in the src/main
    *
    * @return a JUnit TestExcecutionSummary
    */
   private TestExecutionSummary runAnyExceptionTest() {
      LauncherDiscoveryRequest request = LauncherDiscoveryRequestBuilder.request()
            .selectors(
                  selectPackage("net.ihe.gazelle.lib.unittest.adapter"),
                  selectClass(AnyExceptionTest.class)
            )
            .filters(ClassNameFilter.excludeClassNamePatterns("net.ihe.gazelle.lib.unittest.adapter.AnyExceptionTestTest"))
            .build();

      Launcher launcher = LauncherFactory.create();

      SummaryGeneratingListener listener = new SummaryGeneratingListener();
      launcher.registerTestExecutionListeners(listener);

      launcher.execute(request);

      return listener.getSummary();
   }

}
