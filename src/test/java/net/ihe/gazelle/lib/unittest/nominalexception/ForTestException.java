package net.ihe.gazelle.lib.unittest.nominalexception;

/**
 * Class to be targeted to verify the {@link net.ihe.gazelle.lib.unittest.adapter.AnyExceptionTest} is working.
 */
public class ForTestException extends Exception {

   private static final long serialVersionUID = 629372734843682308L;

   /**
    * Constructs a new exception with null as its detail message. The cause is not initialized, and may subsequently be initialized by a call to
    * {@link Throwable#initCause(Throwable)}.
    */
   public ForTestException() {
      super();
   }

   /**
    * Constructs a new exception with the specified detail message. The cause is not initialized, and may subsequently be initialized by a call to
    * {@link Throwable#initCause(Throwable)}.
    *
    * @param message the detail message. Can be retrieved by a later call of {@link Throwable#getMessage()} method.
    */
   public ForTestException(String message) {
      super(message);
   }

   /**
    * Constructs a new exception with the specified detail message and cause. Note that the detail/TransactionRecordingDAO message associated with
    * cause is not automatically incorporated in this exception's detail message.
    *
    * @param message the detail message. Can be retrieved by a later call of {@link Throwable#getMessage()} method.
    * @param cause   the cause. Can be retrieved by a lter call to {@link Throwable#getCause()}. A null value is permitted, and indicates that the
    *                cause is nonexistent or unknown.
    */
   public ForTestException(String message, Throwable cause) {
      super(message, cause);
   }

   /**
    * Constructs a new exception with the specified cause and a detail message of (cause==null ? null : cause.toString()) (which typically contains
    * the class and detail message of cause). This constructor is useful for exceptions that are little more than wrappers for other throwables (for
    * example, {@link java.security.PrivilegedActionException}).
    *
    * @param cause the cause. Can be retrieved by a lter call to {@link Throwable#getCause()}. A null value is permitted, and indicates that the cause
    *              is nonexistent or unknown.
    */
   public ForTestException(Throwable cause) {
      super(cause);
   }

   /**
    * Constructs a new exception with the specified detail message, cause, suppression enabled or disabled, and writable stack trace enabled or
    * disabled.
    *
    * @param message            the detail message. Can be retrieved by a later call of {@link Throwable#getMessage()} method.
    * @param cause              the cause. Can be retrieved by a lter call to {@link Throwable#getCause()}. A null value is permitted, and indicates
    *                           that the cause is nonexistent or unknown.
    * @param enableSuppression  whether or not suppression is enabled or disabled
    * @param writableStackTrace whether or not the stack trace should be writable
    */
   protected ForTestException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
      super(message, cause, enableSuppression, writableStackTrace);
   }
}
