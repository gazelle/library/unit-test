package net.ihe.gazelle.lib.unittest.adapter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.reflections.Reflections;
import org.reflections.ReflectionsException;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.Set;
import java.util.stream.Stream;

/**
 * Test that any exception implements all the constructors of an {@link Exception}.
 *
 * @author ceoche, wbars
 */
public class AnyExceptionTest {

   /**
    * System property to define the package of Exceptions to test
    */
   public static final String PACKAGE_PROPERTY_NAME = "verifyExceptionsInPackage";
   private static final String DEFAULT_PACKAGE = "net.ihe.gazelle";

   private static final String EXCEPTION_MESSAGE = "My exception message";
   private static final String EXCEPTION_CAUSE = "Cause of the exception";

   /**
    * Verify that the exception defined a default public constructor with no arguments
    *
    * @param clazzException the class of the exception to verify
    * @param <T>            A subtype of {@link Exception}
    *
    * @throws InvocationTargetException in case of refelction access error
    * @throws InstantiationException    in case of refelction access error
    */
   @ParameterizedTest
   @MethodSource("getExceptionClasses")
   public <T extends Exception> void createAnException(Class<T> clazzException)
         throws InvocationTargetException, InstantiationException {
      try {
         T exception = clazzException.getDeclaredConstructor().newInstance();
         Assertions.assertNotNull(exception, "It must be possible to create an exception without parameters");
      } catch (NoSuchMethodException | IllegalAccessException e) {
         throw new AssertionError("Exception must expose a public constructor without parameters", e);
      }
   }

   /**
    * Verify that the exception defined a public constructor with a message argument
    *
    * @param clazzException the class of the exception to verify
    * @param <T>            A subtype of {@link Exception}
    *
    * @throws InvocationTargetException in case of refelction access error
    * @throws InstantiationException    in case of refelction access error
    */
   @ParameterizedTest
   @MethodSource("getExceptionClasses")
   public <T extends Exception> void createExceptionWithMessage(Class<T> clazzException)
         throws InvocationTargetException, InstantiationException {
      try {
         T exception = clazzException.getDeclaredConstructor(String.class).newInstance(EXCEPTION_MESSAGE);
         Assertions.assertNotNull(exception, "It must be possible to create an exception with a message");
         Assertions.assertEquals(EXCEPTION_MESSAGE, exception.getMessage());
      } catch (NoSuchMethodException | IllegalAccessException e) {
         throw new AssertionError("Exception must expose a public constructor with parameter (String)", e);
      }
   }

   /**
    * Verify that the exception defined a public constructor with a message and a cause arguments
    *
    * @param clazzException the class of the exception to verify
    * @param <T>            A subtype of {@link Exception}
    *
    * @throws InvocationTargetException in case of refelction access error
    * @throws InstantiationException    in case of refelction access error
    */
   @ParameterizedTest
   @MethodSource("getExceptionClasses")
   public <T extends Exception> void createExceptionWithMessageAndCause(Class<T> clazzException)
         throws InvocationTargetException, InstantiationException {
      RuntimeException cause = new RuntimeException(EXCEPTION_CAUSE);
      try {
         T exception = clazzException.getDeclaredConstructor(String.class, Throwable.class).newInstance(EXCEPTION_MESSAGE, cause);
         Assertions.assertNotNull(exception, "It must be possible to create an exception with a message and a cause");
         Assertions.assertEquals(EXCEPTION_MESSAGE, exception.getMessage());
         Assertions.assertEquals(cause, exception.getCause());
      } catch (NoSuchMethodException | IllegalAccessException e) {
         throw new AssertionError("Exception must expose a public constructor with parameters (String, Throwable)", e);
      }
   }

   /**
    * Verify that the exception defined a public constructor with a cause argument
    *
    * @param clazzException the class of the exception to verify
    * @param <T>            A subtype of {@link Exception}
    *
    * @throws InvocationTargetException in case of refelction access error
    * @throws InstantiationException    in case of refelction access error
    */
   @ParameterizedTest
   @MethodSource("getExceptionClasses")
   public <T extends Exception> void createExceptionWithCause(Class<T> clazzException)
         throws InvocationTargetException, InstantiationException {
      RuntimeException cause = new RuntimeException(EXCEPTION_CAUSE);
      try {
         T exception = clazzException.getDeclaredConstructor(Throwable.class).newInstance(cause);
         Assertions.assertNotNull(exception, "It must be possible to create an exception with a cause");
         Assertions.assertEquals(cause, exception.getCause());
      } catch (NoSuchMethodException | IllegalAccessException e) {
         throw new AssertionError("Exception must expose a public constructor with parameter (Throwable)", e);
      }

   }

   /**
    * Verify that the exception COULD defined a public or protected constructor with a message, a cause and suppression and writeable stack-trace
    * options arguments This test is optional, this explain the usage of JUnit Assumptions.
    *
    * @param clazzException the class of the exception to verify
    * @param <T>            A subtype of {@link Exception}
    *
    * @throws InvocationTargetException in case of refelction access error
    * @throws InstantiationException    in case of refelction access error
    */
   @ParameterizedTest
   @MethodSource("getExceptionClasses")
   public <T extends Exception> void createExceptionWithMessageAndCauseWithSuppressionAndWriteableStackTraceOption(Class<T> clazzException)
         throws InvocationTargetException, InstantiationException {
      RuntimeException cause = new RuntimeException(EXCEPTION_CAUSE);

      Assumptions.assumingThat(() -> {
         try {
            clazzException.getDeclaredConstructor(String.class, Throwable.class, boolean.class, boolean.class);
            return true;
         } catch (NoSuchMethodException e) {
            return false;
         }
      }, () -> {
         Constructor<T> constructor = clazzException.getDeclaredConstructor(String.class, Throwable.class, boolean.class, boolean.class);
         Assumptions.assumingThat(() -> {
            if (Modifier.isProtected(constructor.getModifiers())) {
               constructor.setAccessible(true);
               return true;
            } else {
               return Modifier.isPublic(constructor.getModifiers());
            }
         }, () -> {
            T exception = constructor.newInstance(EXCEPTION_MESSAGE, cause, false, true);
            Assertions.assertNotNull(exception,
                  "If the constructor is defined, it must be possible to create an exception with a message and a cause and specifying if " +
                        "suprression and print stacktrace options are enabled/disabled");
            Assertions.assertEquals(EXCEPTION_MESSAGE, exception.getMessage());
            Assertions.assertEquals(cause, exception.getCause());
         });
      });
   }

   /**
    * Get all exception classes in the class path that starts with the given package defined in the system property {@link
    * AnyExceptionTest#PACKAGE_PROPERTY_NAME} or "net.ihe.gazelle" if not defined.
    *
    * @return a stream of exception classes
    */
   private static Stream<Arguments> getExceptionClasses() {
      try {
         Reflections reflections = new Reflections(getPackageToLookForException());
         Set<Class<? extends Exception>> allGazelleExceptionClasses = reflections.getSubTypesOf(Exception.class);
         allGazelleExceptionClasses.remove(RuntimeException.class);
         return allGazelleExceptionClasses.stream().map(Arguments::of);
      } catch (ReflectionsException e) {
         return Stream.empty();
      }
   }

   /**
    * Retrieve the system property to get the package of exceptions to test.
    *
    * @return the system property value or the default "net.ihe.gazelle" if not defined.
    */
   private static String getPackageToLookForException() {
      String packageFilter = System.getProperty(PACKAGE_PROPERTY_NAME);
      return packageFilter != null ? packageFilter : DEFAULT_PACKAGE;
   }

}
