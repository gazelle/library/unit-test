# Unit Test

This module contains generic unit tests to be used to check all exceptions in your project.

## Exception checking

Simply make sure you defined the `maven-surefire-plugin` with the following configuration in your project :

```xml 
    <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <configuration>
            <dependenciesToScan>
                <dependency>net.ihe.gazelle:lib.unit-test</dependency>
            </dependenciesToScan>
        </configuration>
    </plugin>
```

Add to the `pom.xml` of each module containing exceptions to be checked : 

```xml 
    <dependency>
        <groupId>net.ihe.gazelle</groupId>
        <artifactId>lib.unit-test</artifactId>
        <version>1.0.0-SNAPSHOT</version>
        <scope>test</scope>
    </dependency>
```

### Configure package of Exceptions to check

By default, the module will verify all exception in the class path that starts with `net.ihe
.gazelle`. This can lead to verifying exception of included dependencies. To avoid that you can
 target a more specific package by declaring the environment variable `verifyExceptionsInPackage
 `:
 
 ```xml 
     <plugin>
         <groupId>org.apache.maven.plugins</groupId>
         <artifactId>maven-surefire-plugin</artifactId>
         <configuration>
            <dependenciesToScan>
                <dependency>net.ihe.gazelle:lib.unit-test</dependency>
            </dependenciesToScan>             
            <systemPropertyVariables>
               <verifyExceptionsInPackage>net.ihe.gazelle.app.pdqmconnector</verifyExceptionsInPackage>
            </systemPropertyVariables>
         </configuration>
     </plugin>
 ```

This can be overridden in every sub-module of your maven project :

 ```xml 
     <plugin>
         <groupId>org.apache.maven.plugins</groupId>
         <artifactId>maven-surefire-plugin</artifactId>
         <configuration>           
            <systemPropertyVariables>
               <verifyExceptionsInPackage>net.ihe.gazelle.app.pdqmconnectorservice</verifyExceptionsInPackage>
            </systemPropertyVariables>
         </configuration>
     </plugin>
 ```